﻿// <copyright file="Kijelzo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Asteroid_Game
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    internal class Kijelzo : FrameworkElement
    {
        private ImageBrush hajobrush;
        private ImageBrush urbrush;
        private ImageBrush aszteroidabrush;
        private GameLogic VM;
        private ImageBrush bulletbrush;
        private ImageBrush rabbishbrush;
        private ImageBrush littleasteroidbrush;

        public void Init(GameLogic VM)
        {
            this.VM = VM;
            this.hajobrush = new ImageBrush(new BitmapImage(new Uri("playership.png", UriKind.RelativeOrAbsolute)));
            this.bulletbrush = new ImageBrush(new BitmapImage(new Uri("laserGreen.png", UriKind.RelativeOrAbsolute)));
            this.urbrush = new ImageBrush(new BitmapImage(new Uri("space.png", UriKind.RelativeOrAbsolute)));
            this.aszteroidabrush = new ImageBrush(new BitmapImage(new Uri("asteroid.png", UriKind.RelativeOrAbsolute)));
            this.rabbishbrush = new ImageBrush(new BitmapImage(new Uri("rabbish.png", UriKind.RelativeOrAbsolute)));
            this.littleasteroidbrush = new ImageBrush(new BitmapImage(new Uri("bumm.png", UriKind.RelativeOrAbsolute)));
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.VM != null)
            {
                for (int i = 0; i < this.ActualWidth; i += 256)
                {
                    for (int f = 0; f < this.ActualHeight; f += 256)
                    {
                        drawingContext.DrawRectangle(this.urbrush, null, new Rect(i, f, 256, 256));
                    }
                }

                Rect urhajorect = this.VM.Spaceship.Rectangle;
                this.hajobrush.Transform = new RotateTransform(this.VM.Elfordulas_szoge, urhajorect.X + 25, urhajorect.Y + 25);
                drawingContext.DrawRectangle(this.hajobrush, null, urhajorect);

                foreach (Asteroid item in this.VM.Aszteroidak)
                {
                    drawingContext.DrawRectangle(this.aszteroidabrush, null, item.Rectangle);
                }

                foreach (var item in this.VM.Littleasteroids)
                {
                    drawingContext.DrawRectangle(this.littleasteroidbrush, null, item.Rectangle);
                }

                foreach (Urszemet item in this.VM.Urszemet)
                {
                    drawingContext.DrawRectangle(this.rabbishbrush, null, item.Rectangle);
                }

                foreach (Bullet item in this.VM.Bullets)
                {
                    Rect bullet = new Rect(item.Center.X - 4.5, item.Center.Y - (37 / 2), 9, 37);

                    drawingContext.PushTransform(new RotateTransform(item.Elfordulas_szoge, item.Center.X, item.Center.Y));
                    drawingContext.DrawRectangle(this.bulletbrush, null, bullet);
                    drawingContext.Pop();
                }

                drawingContext.DrawText(
                    new FormattedText(this.VM.Points.ToString(), System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface(new FontFamily("Arial"), FontStyles.Normal, FontWeights.Bold, FontStretches.Normal), 14, Brushes.White), new Point(10, 10));
            }
        }
    }
}