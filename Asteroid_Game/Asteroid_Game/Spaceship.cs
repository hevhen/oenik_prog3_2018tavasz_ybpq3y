﻿// <copyright file="Spaceship.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Asteroid_Game
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    public class Spaceship
    {
        private Point center;
        public Vector speed;
        private Size jatektermeret;

        /// <summary>
        /// Initializes a new instance of the <see cref="Spaceship"/> class.
        /// </summary>
        /// <param name="jatektermeret">Size of the map</param>
        /// <param name="sebesseg">Actual direction of a spaceship</param>
        public Spaceship(Size jatektermeret, Vector sebesseg)
        {
            this.speed = sebesseg;
            this.jatektermeret = jatektermeret;
            this.Elhelyez();
        }

        /// <summary>
        /// Gets a spaceship rectangle
        /// </summary>
        public Rect Rectangle
        {
            get
            {
                return new Rect(this.Center.X - 25, this.Center.Y - 25, 50, 50);
            }
        }

        /// <summary>
        /// Gets or sets the center of a spaceship
        /// </summary>
        public Point Center
        {
            get
            {
                return this.center;
            }

            set
            {
                this.center = value;
            }
        }

        /// <summary>
        /// Gets or sets the direction of a spaceship
        /// </summary>
        public Vector Speed
        {
            get
            {
                return this.speed;
            }

            set
            {
                this.speed = value;
            }
        }

        /// <summary>
        /// Gets or sets the size of the map
        /// </summary>
        public Size Jatekmeret
        {
            get
            {
                return this.jatektermeret;
            }

            set
            {
                this.jatektermeret = value;
            }
        }

        /// <summary>
        /// Determined the state of a spaceship
        /// </summary>
        /// <returns>Returns the state of a spaceship</returns>
        public Point Spaceshipstate()
        {
            return new Point(this.center.X, this.center.Y);
        }

        /// <summary>
        /// Moving the spaceship object(s)
        /// </summary>
        public void Mozgat()
        {
            Point newcenter = new Point(this.Center.X + this.speed.X, this.Center.Y + this.Speed.Y);
            if (newcenter.X >= 0 &&
                newcenter.X <= this.jatektermeret.Width &&
                newcenter.Y >= 0 &&
                newcenter.Y <= this.jatektermeret.Height)
            {
                this.Center = newcenter;
            }
            else
            {
                this.Elhelyez();
            }
        }

        /// <summary>
        /// Declare the spaceship's starting position
        /// </summary>
        public void Elhelyez()
        {
            this.center.X = this.jatektermeret.Width / 2;
            this.center.Y = this.jatektermeret.Height / 2;
        }
    }
}
