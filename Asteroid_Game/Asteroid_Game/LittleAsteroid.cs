﻿// <copyright file="LittleAsteroid.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Asteroid_Game
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    internal class LittleAsteroid
    {
        /// <summary>
        /// Center
        /// </summary>
        private Point center;

        /// <summary>
        /// Sebesseg
        /// </summary>
        private Vector sebesseg;

        /// <summary>
        /// Jaterter mérete
        /// </summary>
        private Size jatektermeret;

        /// <summary>
        /// Initializes a new instance of the <see cref="LittleAsteroid"/> class.
        /// </summary>
        /// <param name="jatektermeret">Size of the map</param>
        /// <param name="center">The center of the littleasteroid</param>
        /// <param name="sebesseg">Speed vector</param>
        public LittleAsteroid(Size jatektermeret, Point center, Vector sebesseg)
        {
            this.jatektermeret = jatektermeret;
            this.Center = center;
            this.sebesseg = sebesseg;
        }

        /// <summary>
        /// Gets or sets the center of a littleasteroid
        /// </summary>
        public Point Center
        {
            get
            {
                return this.center;
            }

            set
            {
                this.center = value;
            }
        }

        /// <summary>
        /// Gets or sets the direction
        /// </summary>
        public Vector Sebesseg
        {
            get
            {
                return this.sebesseg;
            }

            set
            {
                this.sebesseg = value;
            }
        }

        /// <summary>
        /// Gets or sets the size of the map
        /// </summary>
        public Size Jatektermeret
        {
            get
            {
                return this.jatektermeret;
            }

            set
            {
                this.jatektermeret = value;
            }
        }

        /// <summary>
        /// Gets littleAsteroid rectangle
        /// </summary>
        public Rect Rectangle
        {
            get
            {
                return new Rect(this.Center.X - 8, this.Center.Y - 8, 16, 16);
            }
        }

        /// <summary>
        /// Move the littleasteroid object
        /// </summary>
        /// <param name="jatektermeret">The size of the map</param>
        /// <param name="hajo">The rectangle of the spaceship</param>
        /// <returns>Returns the point based on what ot do</returns>
        public int Mozgas(Size jatektermeret, Rect hajo)
        {
            Point newcenter =
                new Point(
                    this.center.X + this.sebesseg.X,
                this.center.Y + this.sebesseg.Y);
            if (this.Rectangle.IntersectsWith(hajo))
            {
                return 5;
            }

            if (newcenter.X >= 0 &&
                newcenter.X <= jatektermeret.Width &&
                newcenter.Y >= 0 &&
                newcenter.Y <= jatektermeret.Height)
            {
                this.center = newcenter;
                return 10;
            }
            else
            {
                return -10;
            }
        }
    }
}
