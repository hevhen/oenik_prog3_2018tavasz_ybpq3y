﻿// <copyright file="GameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Asteroid_Game
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;

    internal class GameLogic
    {
        private double elfordulas_szoge;
        private Size jatektermeret;
        private List<Asteroid> aszteroidak;
        private Spaceship spaceship;
        private List<Urszemet> urszemet;
        private List<Bullet> bullets;
        private List<LittleAsteroid> littleasteroids;
        private int points;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// </summary>
        /// <param name="jatektermeret">Seize of the map</param>
        public GameLogic(Size jatektermeret)
        {
            this.spaceship = new Spaceship(jatektermeret, new Vector(0, -1));
            this.jatektermeret = jatektermeret;
            this.Littleasteroids = new List<LittleAsteroid>();
            this.aszteroidak = new List<Asteroid>();
            this.urszemet = new List<Urszemet>();
            this.Bullets = new List<Bullet>();
            for (int i = 0; i < 5; i++)
            {
                this.aszteroidak.Add(new Asteroid(jatektermeret, this.Littleasteroids));
            }

            for (int i = 0; i < 5; i++)
            {
                this.urszemet.Add(new Urszemet(jatektermeret));
            }
        }

        /// <summary>
        /// Gets or sets the angle of rotation
        /// </summary>
        public double Elfordulas_szoge
        {
            get
            {
                return this.elfordulas_szoge;
            }

            set
            {
                this.elfordulas_szoge = value;
            }
        }

        /// <summary>
        /// Gets or sets the size of the map
        /// </summary>
        public Size Jatektermeret
        {
            get
            {
                return this.jatektermeret;
            }

            set
            {
                this.jatektermeret = value;
            }
        }

        /// <summary>
        /// Gets or sets a spaceship
        /// </summary>
        internal Spaceship Spaceship
        {
            get
            {
                return this.spaceship;
            }

            set
            {
                this.spaceship = value;
            }
        }

        /// <summary>
        /// Gets or sets a list of asteroids
        /// </summary>
        internal List<Asteroid> Aszteroidak
        {
            get
            {
                return this.aszteroidak;
            }

            set
            {
                this.aszteroidak = value;
            }
        }

        /// <summary>
        /// Gets of sets a list of bullets
        /// </summary>
        internal List<Bullet> Bullets
        {
            get
            {
                return this.bullets;
            }

            set
            {
                this.bullets = value;
            }
        }

        /// <summary>
        /// Gets or sets the points
        /// </summary>
        internal int Points
        {
            get
            {
                return this.points;
            }

            set
            {
                this.points = value;
            }
        }

        /// <summary>
        /// Gets or sets an urszemet list
        /// </summary>
        internal List<Urszemet> Urszemet
        {
            get
            {
                return this.urszemet;
            }

            set
            {
                this.urszemet = value;
            }
        }

        /// <summary>
        /// Gets or sets a littleasteroid list
        /// </summary>
        internal List<LittleAsteroid> Littleasteroids
        {
            get
            {
                return this.littleasteroids;
            }

            set
            {
                this.littleasteroids = value;
            }
        }

        /// <summary>
        /// Main moving
        /// </summary>
        public void Mozgat()
        {
            this.Spaceship.Mozgat();
            Rect urhajo_teglalap = this.Spaceship.Rectangle;
            for (int i = 0; i < this.Bullets.Count; i++)
            {
                bool belulvan = this.Bullets[i].Mozgas(this.jatektermeret);
                if (!belulvan)
                {
                    this.Bullets.Remove(this.Bullets[i]);
                }
            }

            for (int i = 0; i < this.littleasteroids.Count; i++)
            {
                if (this.littleasteroids[i].Mozgas(this.jatektermeret, urhajo_teglalap) == 10)
                {
                    this.littleasteroids[i].Mozgas(this.jatektermeret, urhajo_teglalap);
                }
                else if (this.littleasteroids[i].Mozgas(this.jatektermeret, urhajo_teglalap) == -10)
                {
                    this.littleasteroids.Remove(this.littleasteroids[i]);
                }
                else if (this.littleasteroids[i].Mozgas(this.jatektermeret, urhajo_teglalap) == 5)
                {
                    this.spaceship.Center = new Point(this.jatektermeret.Width / 2, this.jatektermeret.Height / 2);
                }
            }

            for (int i = 0; i < this.Urszemet.Count; i++)
            {
                int pont = this.urszemet[i].Mozgat(this.bullets, urhajo_teglalap);
                if (pont == -10)
                {
                    this.spaceship.Center = new Point(this.jatektermeret.Width / 2, this.jatektermeret.Height / 2);
                }
            }

            for (int i = 0; i < this.Aszteroidak.Count; i++)
            {
                int pont = this.Aszteroidak[i].Mozgat(this.Bullets, urhajo_teglalap);
                if (pont == -10)
                {
                    this.spaceship.Center = new Point(this.jatektermeret.Width / 2, this.jatektermeret.Height / 2);
                }
                else if (pont == 10)
                {
                    this.points++;
                }
            }
        }

        /// <summary>
        /// Keyboard keydown
        /// </summary>
        /// <param name="k">Keydown</param>
        public void BillentyuNyomas(Key k)
        {
            if (k == Key.Left)
            {
                this.elfordulas_szoge -= 10;
                double rad = (this.elfordulas_szoge - 90) *
                    (Math.PI / 180);
                double dx = Math.Cos(rad);
                double dy = Math.Sin(rad);

                this.Spaceship.speed.X = dx * 6;
                this.Spaceship.speed.Y = dy * 6;
            }
            else if (k == Key.Right)
            {
                this.elfordulas_szoge += 10;
                double rad = (this.elfordulas_szoge - 90) *
                    (Math.PI / 180);
                double dx = Math.Cos(rad);
                double dy = Math.Sin(rad);

                this.Spaceship.speed.X = dx * 6;
                this.Spaceship.speed.Y = dy * 6;
            }
            else if (k == Key.Up)
            {
                double rad = (this.elfordulas_szoge - 90) *
                    (Math.PI / 180);
                double dx = Math.Cos(rad);
                double dy = Math.Sin(rad);

                this.Spaceship.speed.X = dx * 16;
                this.Spaceship.speed.Y = dy * 16;
            }
            else if (k == Key.Space)
            {
                double rad = (this.elfordulas_szoge - 90) *
                    (Math.PI / 180);
                double dx = Math.Cos(rad);
                double dy = Math.Sin(rad);
                dx = dx * 28;
                dy = dy * 28;
                this.Bullets.Add(new Bullet(this.spaceship.Spaceshipstate(), new Vector(dx, dy), this.elfordulas_szoge));
            }
        }
    }
}
