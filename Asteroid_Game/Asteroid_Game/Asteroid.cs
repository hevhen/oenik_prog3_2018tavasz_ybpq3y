﻿// <copyright file="Asteroid.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Asteroid_Game
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    internal class Asteroid
    {
        /// <summary>
        /// Random
        /// </summary>
        private static Random r = new Random();

        /// <summary>
        /// Center
        /// </summary>
        private Point center;

        /// <summary>
        /// Sebesség
        /// </summary>
        private Vector sebesseg;

        /// <summary>
        /// Jaték mérete
        /// </summary>
        private Size jatekmeret;

        /// <summary>
        /// Kis Aszteroida lista
        /// </summary>
        private List<LittleAsteroid> littleasteroids;

        /// <summary>
        /// Initializes a new instance of the <see cref="Asteroid"/> class.
        /// </summary>
        /// <param name="jatektermeret">Size of the map</param>
        /// <param name="egy">One Littleasteroid</param>
        public Asteroid(Size jatektermeret, List<LittleAsteroid> egy)
        {
            this.Littleasteroids = egy;
            this.jatekmeret = jatektermeret;
            this.Elhelyez();
        }

        /// <summary>
        /// Gets an asteroid rectangle
        /// </summary>
        public Rect Rectangle
        {
            get
            {
                return new Rect(this.Center.X - 25, this.Center.Y - 25, 50, 50);
            }
        }

        /// <summary>
        /// Gets or sets the center
        /// </summary>
        public Point Center
        {
            get
            {
                return this.center;
            }

            set
            {
                this.center = value;
            }
        }

        /// <summary>
        /// Gets or sets a littleasteroid list
        /// </summary>
        internal List<LittleAsteroid> Littleasteroids
        {
            get
            {
                return this.littleasteroids;
            }

            set
            {
                this.littleasteroids = value;
            }
        }

        /// <summary>
        /// Spawns Asteroids
        /// </summary>
        public void Elhelyez()
        {
            int vel = r.Next(0, 4);
            switch (vel)
            {
                case 0:
                    this.Center = new Point(r.Next(25, (int)this.jatekmeret.Width - 25), 25);
                    this.sebesseg = new Vector(this.Randomizalo(-6, 6), this.Randomizalo(1, 6));
                    break;
                case 1:
                    this.Center = new Point(r.Next(25, (int)this.jatekmeret.Width - 25), this.jatekmeret.Height - 25);
                    this.sebesseg = new Vector(this.Randomizalo(-6, 6), this.Randomizalo(-6, -1));
                    break;
                case 2:
                    this.Center = new Point(25, r.Next(25, (int)this.jatekmeret.Height - 25));
                    this.sebesseg = new Vector(this.Randomizalo(0, 6), this.Randomizalo(-6, 6));
                    break;
                case 3:
                    this.Center = new Point(this.jatekmeret.Width - 25, r.Next(25, (int)this.jatekmeret.Height - 25));
                    this.sebesseg = new Vector(this.Randomizalo(-6, -1), this.Randomizalo(-6, 6));
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Spawns 4 little asteroid after the main destroyed
        /// </summary>
        /// <param name="asteroida">The asteroid that destroyed</param>
        public void Szetrobban(Asteroid asteroida)
        {
            this.Littleasteroids.Add(new LittleAsteroid(this.jatekmeret, asteroida.center, asteroida.sebesseg));
            this.Littleasteroids.Add(new LittleAsteroid(this.jatekmeret, asteroida.center, new Vector(asteroida.sebesseg.X, asteroida.sebesseg.Y * -1)));
            this.Littleasteroids.Add(new LittleAsteroid(this.jatekmeret, asteroida.center, new Vector(asteroida.sebesseg.X * -1, asteroida.sebesseg.Y)));
            this.Littleasteroids.Add(new LittleAsteroid(this.jatekmeret, asteroida.center, new Vector(asteroida.sebesseg.X * -1, asteroida.sebesseg.Y * -1)));
        }

        /// <summary>
        /// Moving the asteroids and checking the intersects
        /// </summary>
        /// <param name="bullets">List of the bullets</param>
        /// <param name="hajo">Rect of the players spaceship</param>
        /// <returns>Return point that specify the output</returns>
        public int Mozgat(List<Bullet> bullets, Rect hajo)
        {
            Rect aszteroidarect = this.Rectangle;
            foreach (Bullet item in bullets)
            {
                Rect lovedekrect = new Rect(item.Center.X - (9 / 2), item.Center.Y - (37 / 2), 9, 37);
                if (lovedekrect.IntersectsWith(aszteroidarect))
                {
                    this.Szetrobban(this);
                    this.Elhelyez();
                    return 10;
                }
            }

            if (aszteroidarect.IntersectsWith(hajo))
            {
                return -10;
            }

            Point newcenter = new Point(this.Center.X + this.sebesseg.X, this.Center.Y + this.sebesseg.Y);
            if (newcenter.X >= 0 &&
                newcenter.X <= this.jatekmeret.Width &&
                newcenter.Y >= 0 &&
                newcenter.Y <= this.jatekmeret.Height)
            {
                this.Center = newcenter;
            }
            else
            {
                this.Elhelyez();
            }

            return 0;
        }

        /// <summary>
        /// Random number generator
        /// </summary>
        /// <param name="min">The minimum</param>
        /// <param name="max">The maximum</param>
        /// <returns>Returns a random number between min and max params</returns>
        private int Randomizalo(int min, int max)
        {
            int vel = 0;
            do
            {
                vel = r.Next(min, max + 1);
            }
            while (vel == 0);
            return vel;
        }
    }
}
