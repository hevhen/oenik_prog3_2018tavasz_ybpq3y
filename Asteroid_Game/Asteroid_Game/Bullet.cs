﻿// <copyright file="Bullet.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Asteroid_Game
{
    using System.Windows;

    internal class Bullet
    {
        private Point center;
        private Vector sebesseg;
        private double elfordulasSzoge;

        /// <summary>
        /// Initializes a new instance of the <see cref="Bullet"/> class.
        /// </summary>
        /// <param name="center">Center of a Bullet</param>
        /// <param name="sebesseg">direction of a bullet</param>
        /// <param name="elf">Angle of rotation</param>
        public Bullet(Point center, Vector sebesseg, double elf)
        {
            this.elfordulasSzoge = elf;
            this.Center = center;
            this.Sebesseg = sebesseg;
        }

        /// <summary>
        /// Gets or sets the center of a bullet
        /// </summary>
        public Point Center
        {
            get
            {
                return this.center;
            }

            set
            {
                this.center = value;
            }
        }

        /// <summary>
        /// Gets or sets the direction
        /// </summary>
        public Vector Sebesseg
        {
            get
            {
                return this.sebesseg;
            }

            set
            {
                this.sebesseg = value;
            }
        }

        /// <summary>
        /// Gets or sets the angle of rotation
        /// </summary>
        public double Elfordulas_szoge
        {
            get
            {
                return this.elfordulasSzoge;
            }

            set
            {
                this.elfordulasSzoge = value;
            }
        }

        /// <summary>
        /// Moving the bullets
        /// </summary>
        /// <param name="jatektermeret">Size of the map</param>
        /// <returns>True or false based on the new point is in the map</returns>
        public bool Mozgas(Size jatektermeret)
        {
            Point newcenter =
                new Point(this.center.X + this.sebesseg.X, this.center.Y + this.sebesseg.Y);
            if (newcenter.X >= 0 &&
                newcenter.X <= jatektermeret.Width &&
                newcenter.Y >= 0 &&
                newcenter.Y <= jatektermeret.Height)
            {
                this.center = newcenter;
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
