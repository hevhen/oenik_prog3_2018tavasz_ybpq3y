﻿// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Asteroid_Game
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;
    using System.Windows.Threading;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private GameLogic VM;
        private DispatcherTimer dt;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.VM = new GameLogic(new Size(this.kijelzo.ActualWidth, this.kijelzo.ActualHeight));
            this.kijelzo.Init(this.VM);
            this.dt = new DispatcherTimer();
            this.dt.Interval = TimeSpan.FromMilliseconds(20);
            this.dt.Tick += this.Dt_Tick;
            this.dt.Start();
        }

        private void Dt_Tick(object sender, EventArgs e)
        {
            this.VM.Mozgat();
            this.kijelzo.InvalidateVisual();
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.VM = new GameLogic(new Size(this.kijelzo.ActualWidth, this.kijelzo.ActualHeight));
            this.kijelzo.Init(this.VM);
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            this.VM.BillentyuNyomas(e.Key);
            this.kijelzo.InvalidateVisual();
        }
    }
}
