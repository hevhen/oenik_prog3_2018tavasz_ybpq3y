﻿// <copyright file="Urszemet.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Asteroid_Game
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    class Urszemet
    {
        private Point center;
        private Vector sebesseg;
        private static Random r = new Random();
        private Size jatekmeret;

        /// <summary>
        /// Initializes a new instance of the <see cref="Urszemet"/> class.
        /// </summary>
        /// <param name="jatektermeret">Size of the map</param>
        public Urszemet(Size jatektermeret)
        {
            this.jatekmeret = jatektermeret;
            this.Elhelyez();
        }

        /// <summary>
        /// Gets a spacerubbish's Rectangle
        /// </summary>
        public Rect Rectangle
        {
            get
            {
                return new Rect(
                    this.Center.X - 25, this.Center.Y - 25, 50, 50);
            }
        }

        /// <summary>
        /// Gets or sets the center
        /// </summary>
        public Point Center
        {
            get
            {
                return this.center;
            }

            set
            {
                this.center = value;
            }
        }

        /// <summary>
        /// Spawns Asteroids
        /// </summary>
        public void Elhelyez()
        {
            int vel = r.Next(0, 4);
            switch (vel)
            {
                case 0:
                    this.Center = new Point(r.Next(25, (int)this.jatekmeret.Width - 25), 25);
                    this.sebesseg = new Vector(this.Randomizalo(-6, 6), this.Randomizalo(1, 6));
                    break;
                case 1:
                    this.Center = new Point(r.Next(25, (int)this.jatekmeret.Width - 25), this.jatekmeret.Height - 25);
                    this.sebesseg = new Vector(this.Randomizalo(-6, 6), this.Randomizalo(-6, -1));
                    break;
                case 2:
                    this.Center = new Point(25, r.Next(25, (int)this.jatekmeret.Height - 25));
                    this.sebesseg = new Vector(this.Randomizalo(0, 6), this.Randomizalo(-6, 6));
                    break;
                case 3:
                    this.Center = new Point(this.jatekmeret.Width - 25, r.Next(25, (int)this.jatekmeret.Height - 25));
                    this.sebesseg = new Vector(this.Randomizalo(-6, -1), this.Randomizalo(-6, 6));
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Moving the Spacerabbish objects
        /// </summary>
        /// <param name="bullets">List of the bullets</param>
        /// <param name="hajo">Spaceship Rectangle</param>
        /// <returns>Returns points</returns>
        public int Mozgat(List<Bullet> bullets, Rect hajo)
        {
            Rect urszemetrect = new Rect(this.Center.X - 10, this.Center.Y - 10, 20, 20);
            for (int i = 0; i < bullets.Count; i++)
            {
                Rect lovedekrect = new Rect(bullets[i].Center.X - 5, bullets[i].Center.Y - 5, 10, 10);
                if (lovedekrect.IntersectsWith(this.Rectangle))
                {
                    bullets.Remove(bullets[i]);
                    return 10;
                }
            }

            if (this.Rectangle.IntersectsWith(hajo))
            {
                return -10;
            }

            Point newcenter = new Point(this.Center.X + (this.sebesseg.X / 2), this.Center.Y + (this.sebesseg.Y / 2));
            if (newcenter.X >= 0 &&
                newcenter.X <= this.jatekmeret.Width &&
                newcenter.Y >= 0 &&
                newcenter.Y <= this.jatekmeret.Height)
            {
                this.Center = newcenter;
            }
            else
            {
                this.Elhelyez();
            }

            return 0;
        }

        /// <summary>
        /// Random number generator between min and max
        /// </summary>
        /// <param name="min">Minimum</param>
        /// <param name="max">Maximum</param>
        /// <returns>Returns a random number</returns>
        private int Randomizalo(int min, int max)
        {
            int vel = 0;
            do
            {
                vel = r.Next(min, max + 1);
            }
            while (vel == 0);
            return vel;
        }
    }
}